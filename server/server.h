#ifndef __MAILCHECKSERVER_H__
#define __MAILCHECKSERVER_H__

#include "mailcheck.h"
#include "GnomeMailbox.h"

/* Internal structure definitions */

struct _CorbaRefs
{
  CORBA_ORB orb;
  PortableServer_POA poa;
  CORBA_Environment ev;
};

/*  typedef GList* MailboxList; */
/*  typedef ClientInfo** ClientList; */

typedef struct _MailboxType MailboxType;
typedef struct _MailboxInfo MailboxInfo;
typedef struct _ClientInfo ClientInfo;
typedef int (*PollFunc)(GnomeMailbox* box);
typedef gboolean (*TryAuthFunc)(gpointer box, gchar* passwd);
typedef gchar* (*ExtractFunc)(gpointer obj);

struct _MailboxType
{
  gchar* name;
  PollFunc poll_func;
  TryAuthFunc try_auth_func;
};

struct _MailboxInfo
{
  gchar* name;
  int type;
  unsigned long int interval;
  GList* monitors;
  GnomeMailbox* obj;
};

struct _ClientInfo
{
  gchar* name;
  gchar* ior;
  int id;
};

/* Declarations of global data structures */

extern struct _CorbaRefs CorbaRefs;
extern GList* client_list;
extern GList* mailbox_list;
extern MailboxType* mailbox_types[];
extern gint num_types;
extern gint num_clients;
extern gint num_mailboxes;
extern gint nextClientID;

/* Function prototypes */

/* CORBA implementation functions */

GNOME_MailCheckServer_stringList*
do_GNOME_MailCheckServer_list_mailboxes(PortableServer_Servant servant,
					CORBA_Environment* ev);

GNOME_MailCheckServer_stringList*
do_GNOME_MailCheckServer_list_mailbox_types(PortableServer_Servant servant,
					    CORBA_Environment* ev);

GNOME_MailCheckServer_ClientID
do_GNOME_MailCheckServer_register_client(PortableServer_Servant servant,
					 const CORBA_char* name,
					 const CORBA_char* ior,
					 CORBA_Environment* ev);

CORBA_boolean
do_GNOME_MailCheckServer_unregister_client(PortableServer_Servant servant,
					   const GNOME_MailCheckServer_ClientID id,
					   CORBA_Environment* ev);

CORBA_boolean
do_GNOME_MailCheckServer_set_monitor_status(PortableServer_Servant servant,
					    const GNOME_MailCheckServer_ClientID client,
					    const CORBA_char* mailbox,
					    const CORBA_boolean status,
					    CORBA_Environment* ev);

CORBA_boolean
do_GNOME_MailCheckServer_get_monitor_status(PortableServer_Servant servant,
					    const GNOME_MailCheckServer_ClientID client,
					    const CORBA_char* mailbox,
					    CORBA_Environment* ev);

GNOME_MailCheckServer_Error
do_GNOME_MailCheckServer_create_mailbox(PortableServer_Servant servant,
					const CORBA_char* type,
					const CORBA_char* name,
					CORBA_Environment* ev);

CORBA_boolean
do_GNOME_MailCheckServer_destroy_mailbox(PortableServer_Servant servant,
					 const CORBA_char* name,
					 CORBA_Environment* ev);

CORBA_boolean
do_GNOME_MailCheckServer_set_check_interval(PortableServer_Servant servant,
					    const CORBA_char* name,
					    const CORBA_unsigned_long secs,
					    CORBA_Environment* ev);

CORBA_boolean
do_GNOME_MailCheckServer_destroy_mailbox(PortableServer_Servant servant,
					 const CORBA_char* name,
					 CORBA_Environment* ev);

CORBA_unsigned_long
do_GNOME_MailCheckServer_get_check_interval(PortableServer_Servant servant,
					    const CORBA_char* name,
					    CORBA_Environment* ev);

CORBA_boolean
do_GNOME_MailCheckServer_authenticate_mailbox(PortableServer_Servant servant,
					      const CORBA_char* name,
					      const CORBA_char* password,
					      CORBA_Environment* ev);

/* sequence utility routines */

GNOME_MailCheckServer_stringList* 
array_to_corba_seq(void* array[], int num, ExtractFunc func);
GNOME_MailCheckServer_stringList* 
glist_to_corba_seq(GList* list, ExtractFunc func);
GList* corba_seq_to_glist(GNOME_MailCheckServer_stringList* seq);

/* comparators / extractors / convertors */
gint client_id_comp(gconstpointer a, gconstpointer b);
gint mailbox_name_comp(gconstpointer a, gconstpointer b);
gchar* extract_type_name(MailboxType* type);
gchar* extract_box_name(MailboxInfo* info);

/* memory management routines */
gboolean dealloc_client_struct(ClientInfo* client, gboolean free_client);
gboolean dealloc_mailbox_struct(MailboxInfo* box, gboolean free_box);

/* client notification routines */
void notify_new_mailbox(ClientInfo* client, gchar* mailbox);
void notify_delete_mailbox(ClientInfo* client, gchar* mailbox);
void notify_server_shutdown(ClientInfo* client, gchar* dummy);

/* other stuff */
gint find_mailbox_type(gchar* type);
gchar* init_CORBA(gint* argc, gchar** argv);

#endif

