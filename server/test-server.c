#include <gnome.h>
#include <glib.h>
#include <orb/orbit.h>
#include <libgnorba/gnorba.h>
#include "server.h"

CORBA_ORB orb;
PortableServer_Servant poa;
GNOME_MailCheckServer mcs;
CORBA_Environment ev;

void print_string_list(GList* list)
{
  while(list)
    {
      printf("%s\n", (char*) list->data);
      list = g_list_next(list);
    }
}  
  
int test_types()
{
  GList *list = NULL;
  list = corba_seq_to_glist(GNOME_MailCheckServer_list_mailbox_types(mcs,
								     &ev));
  printf("Listing mailbox types.  Should be dummy1, dummy2, lastdummy.\n");
  print_string_list(list);
  g_list_free(list);
  return 0;
}

int test_mailbox()
{
  GNOME_MailCheckServer_Error err;
  CORBA_boolean res;
  GList* list = NULL;
  int errors = 0;
  printf("Creating mailbox foo1 of type dummy1... ");
  err = GNOME_MailCheckServer_create_mailbox(mcs, "dummy1", "foo1", &ev);
  printf("Returned %d: ",err);
  if(err != GNOME_MailCheckServer_OK) 
    {
      printf("Ooops!\n");
      errors++;
    }
  else printf("OK.\n");
  printf("Creating mailbox foo2 of type dummy2... ");
  err = GNOME_MailCheckServer_create_mailbox(mcs, "dummy2", "foo2", &ev);
  printf("Returned %d: ",err);
  if(err != GNOME_MailCheckServer_OK) 
    {
      printf("Ooops!\n");
      errors++;
    }
  else printf("OK.\n");
  printf("Creating mailbox foo3 of type dummy2... ");
  err = GNOME_MailCheckServer_create_mailbox(mcs, "dummy2", "foo3", &ev);
  printf("Returned %d: ",err);
  if(err != GNOME_MailCheckServer_OK) 
    {
      printf("Ooops!\n");
      errors++;
    }
  else printf("OK.\n");
  printf("Trying to create mailbox foo1 of type lastdummy... ");
  err = GNOME_MailCheckServer_create_mailbox(mcs, "lastdummy", "foo1", &ev);
  printf("Returned %d: ",err);
  if(err != GNOME_MailCheckServer_EXISTS) 
    {
      printf("Ooops!\n");
      errors++;
    }
  else printf("OK.\n");
  printf("Trying to create mailbox bad1 of type badtype... ");
  err = GNOME_MailCheckServer_create_mailbox(mcs, "badtype", "bad1", &ev);
  printf("Returned %d: ",err);
  if(err != GNOME_MailCheckServer_NOTFOUND) 
    {
      printf("Ooops!\n");
      errors++;
    }
  else printf("OK.\n");
  printf("Destroying mailbox foo3... ");
  res = GNOME_MailCheckServer_destroy_mailbox(mcs, "foo3", &ev);
  printf("Returned %d: ",res);
  if(res != CORBA_TRUE) 
    {
      printf("Ooops!\n");
      errors++;
    }
  else printf("OK.\n");

  printf("Listing mailboxes: \n");
  list = corba_seq_to_glist(GNOME_MailCheckServer_list_mailboxes(mcs,
								 &ev));
  print_string_list(list);
  g_list_free(list);
  
  return errors;
}

int test_register()
{
  return 0;
}

int test_monitor()
{
  return 0;
}

int test_interval()
{
  return 0;
}

int test_auth()
{
  return 0;
}

int test_config()
{
  return 0;
}

			    
int main(int argc, char* argv[])
{
  int errors = 0;

  if(argc != 2)
    {
      g_error("Usage: %s <IOR>\n", argv[0]);
    }
  
  CORBA_exception_init(&ev);
  orb = gnome_CORBA_init("server-test", "0.1.0", &argc, argv, 0, &ev);
  poa = CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);
  mcs = CORBA_ORB_string_to_object(orb, argv[1], &ev);

  printf("*** Beginning tests ***\n");
  errors += test_types();
  errors += test_mailbox();
  errors += test_register();
  errors += test_monitor();
  errors += test_interval();
  errors += test_auth();
  errors += test_config();

  printf("\n*** Total errors: %d ***\n", errors);
  
  CORBA_Object_release(mcs, &ev);

  if(errors) return 1;

  return 0;
}







