/* Definitions of global data structures */

#include <glib.h>
#include <orb/orbit.h>
#include "server.h"

/* the structure holding the server's assorted CORBA data: the ORB
   reference, POA reference, etc. */

struct _CorbaRefs CorbaRefs;

/* the list of all clients currently registered with the server */

GList* client_list = NULL;
gint num_clients = 0;
gint nextClientID = 0;

/* the list of all mailboxes currently registered with the server */

GList* mailbox_list = NULL;
gint num_mailboxes = 0;

