#include <glib.h>
#include "server.h"

gchar* extract_type_name(MailboxType* type)
{
  return type->name;
}

gchar* extract_box_name(MailboxInfo* info)
{
  return info->name;
}

gint client_id_comp(gconstpointer a, gconstpointer b)
{
  ClientInfo* client = (ClientInfo*) a;
  gint* id = (gint*) b;
  if(client->id > *id) return 1;
  if(client->id < *id) return -1;
  return 0;
}

gint mailbox_name_comp(gconstpointer a, gconstpointer b)
{
  MailboxInfo* box = (MailboxInfo*) a;
  gchar* name = (gchar*) b;
  return strcmp(box->name, name);
}

gboolean dealloc_client_struct(ClientInfo* client, gboolean free_client)
{
  if(!client) return FALSE;
  g_free(client->name);
  g_free(client->ior);
  if(free_client) g_free(client);
  return TRUE;
}

gboolean dealloc_mailbox_struct(MailboxInfo* box, gboolean free_box)
{
  if(!box) return FALSE;
  g_free(box->name);
  if(free_box) g_free(box);
  return TRUE;
}



