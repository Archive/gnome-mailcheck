/*
The GNOMEMailCheck CORBA server
Copyright (C) Russell Steinthal 1999
*/

#include <glib.h>
#include "server.h"

#define DEFAULT_CHECK_INTERVAL 300

/*  GCompareFunc client_id_comp; */

GNOME_MailCheckServer_stringList*
do_GNOME_MailCheckServer_list_mailboxes(PortableServer_Servant servant,
					CORBA_Environment* ev)
{
  return glist_to_corba_seq(mailbox_list, extract_box_name);
}


GNOME_MailCheckServer_stringList*
do_GNOME_MailCheckServer_list_mailbox_types(PortableServer_Servant servant,
					    CORBA_Environment* ev)
{
  return array_to_corba_seq((void**)mailbox_types, num_types, extract_type_name);
}

GNOME_MailCheckServer_ClientID
do_GNOME_MailCheckServer_register_client(PortableServer_Servant servant,
					 const CORBA_char* name,
					 const CORBA_char* ior,
					 CORBA_Environment* ev)
{
  ClientInfo* newClient = g_new(ClientInfo, 1);
  newClient->name = g_strdup(name);
  newClient->ior  = g_strdup(ior);
  newClient->id   = nextClientID++;
  client_list = g_list_append(client_list, newClient);
  num_clients++;
#ifdef DEBUG
  g_message("Registered client %s with id %d.\n", name, newClient->id);
#endif
  return newClient->id;
}

CORBA_boolean
do_GNOME_MailCheckServer_unregister_client(PortableServer_Servant servant,
					   const 
					   GNOME_MailCheckServer_ClientID _id,
					   CORBA_Environment* ev)
{
  GNOME_MailCheckServer_ClientID id = _id;
  GList* target = g_list_find_custom(client_list, &id, client_id_comp);
  if(target)     
    {
      client_list = g_list_remove_link(client_list, target);
#ifdef DEBUG
      g_message("Unregistered client %s with id %d.\n", 
		((ClientInfo*)(target->data))->name, id);
#endif
      dealloc_client_struct(target->data, TRUE);
      num_clients--;
      return CORBA_TRUE;
    }
#ifdef DEBUG
  g_message("Could not find client with id %d.\n", id);
#endif
  return CORBA_FALSE;
}


CORBA_boolean
do_GNOME_MailCheckServer_set_monitor_status(PortableServer_Servant servant,
					    const GNOME_MailCheckServer_ClientID client,
					    const CORBA_char* mailbox,
					    const CORBA_boolean status,
					    CORBA_Environment* ev)
{
  MailboxInfo* box = NULL;
  ClientInfo* cinf = NULL;
  GList* target = NULL;
  GNOME_MailCheckServer_ClientID id = client;
  target = g_list_find_custom(mailbox_list, (CORBA_char*) mailbox, mailbox_name_comp);
  if(!target) return CORBA_FALSE;
  box = target->data;

  /* check to see if the client is currently monitoring the box */
  target = g_list_find_custom(box->monitors, &id, client_id_comp);
  if(target)
    {
      if(!status) g_list_remove_link(box->monitors, target);
      return CORBA_TRUE;
    }

  /* the given client was not found on the monitor list */

  if(status)
    {
      target = g_list_find_custom(client_list, &id, client_id_comp);
      cinf = target->data;
      g_list_append(box->monitors, cinf);
    }
  
  return CORBA_TRUE;
}

CORBA_boolean
do_GNOME_MailCheckServer_get_monitor_status(PortableServer_Servant servant,
					    const GNOME_MailCheckServer_ClientID client,
					    const CORBA_char* mailbox,
					    CORBA_Environment* ev)
{
  GList* mi = NULL;
  GList* ci = NULL;
  GNOME_MailCheckServer_ClientID id = client;
  mi = g_list_find_custom(mailbox_list, (CORBA_char*) mailbox, 
			  mailbox_name_comp);
  if(!mi) return CORBA_FALSE;
  ci = g_list_find_custom(((MailboxInfo*)(mi->data))->monitors, 
			  &id, client_id_comp);
  if(ci) return CORBA_TRUE;
  return CORBA_FALSE;
}

/*
  Add a new mailbox to the server's list with the given name and given
  type; returns true if successful, false otherwise.

  Failure could mean: out-of-memory or name in use (this should
  probably be changed to an enum return code)
*/

GNOME_MailCheckServer_Error
do_GNOME_MailCheckServer_create_mailbox(PortableServer_Servant servant,
					const CORBA_char* type,
					const CORBA_char* name,
					CORBA_Environment* ev)
{
  MailboxInfo* box = g_new(MailboxInfo, 1);
  if(!box) return GNOME_MailCheckServer_NOMEM;
  if(g_list_find_custom(mailbox_list, (CORBA_char*) name, mailbox_name_comp))
    return GNOME_MailCheckServer_EXISTS;
  box->type = find_mailbox_type((CORBA_char*)type);
  if(box->type < 0)
    {
      g_free(box);
      return GNOME_MailCheckServer_NOTFOUND;
    }
  box->name = g_strdup(name);
  box->interval = DEFAULT_CHECK_INTERVAL;
  box->monitors = NULL;
  mailbox_list = g_list_append(mailbox_list, box);
  num_mailboxes++;
  
  /* Notify clients via CORBA */
  g_list_foreach(client_list, (GFunc) notify_new_mailbox, (CORBA_char*) name);

  return GNOME_MailCheckServer_OK;
}

CORBA_boolean
do_GNOME_MailCheckServer_destroy_mailbox(PortableServer_Servant servant,
					 const CORBA_char* name,
					 CORBA_Environment* ev)
{
  MailboxInfo* box = NULL;
  GList* target = NULL;
  target = g_list_find_custom(mailbox_list, (CORBA_char*) name, 
			      mailbox_name_comp);
  if(target)
    box = target->data;
  else 
    return CORBA_FALSE;
  mailbox_list = g_list_remove_link(mailbox_list, target);
  /* Notify clients monitoring this mailbox via CORBA */
  g_list_foreach(box->monitors, (GFunc) notify_delete_mailbox, 
		 (CORBA_char*) name);
  dealloc_mailbox_struct(box, TRUE);
  num_mailboxes--;
  return CORBA_TRUE;
}

CORBA_boolean
do_GNOME_MailCheckServer_set_check_interval(PortableServer_Servant servant,
					    const CORBA_char* name,
					    const CORBA_unsigned_long secs,
					    CORBA_Environment* ev)
{
  MailboxInfo* box = NULL;
  GList* target = NULL;
  target = g_list_find_custom(mailbox_list, (CORBA_char*) name, 
			      mailbox_name_comp);
  if(!target) return CORBA_FALSE;
  box = target->data;
  box->interval = secs;
  return CORBA_TRUE;
}


CORBA_unsigned_long
do_GNOME_MailCheckServer_get_check_interval(PortableServer_Servant servant,
					    const CORBA_char* name,
					    CORBA_Environment* ev)
{
  MailboxInfo* box = NULL;
  GList* target = NULL;
  target = g_list_find_custom(mailbox_list, (CORBA_char*) name, 
			      mailbox_name_comp);
  if(!target) return 0;
  return box->interval;
}

CORBA_boolean
do_GNOME_MailCheckServer_authenticate_mailbox(PortableServer_Servant servant,
					      const CORBA_char* name,
					      const CORBA_char* password,
					      CORBA_Environment* ev)
{
  gboolean result;
  MailboxInfo* box = NULL;
  GList* target = NULL;
  GnomeMailbox* gnomeMailbox = NULL;
  target = g_list_find_custom(mailbox_list, (CORBA_char*) name, 
			      mailbox_name_comp);
  if(!target) return CORBA_FALSE;
  box = target->data;
  result = mailbox_types[box->type]->try_auth_func(box, (CORBA_char*)password);
  return (result ? CORBA_TRUE : CORBA_FALSE);
}













