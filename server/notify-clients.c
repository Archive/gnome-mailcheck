/*
  The functions in this file handle the notification of clients of
  server events through the CORBA mechanism (specifically, through
  invocations of functions in the GNOME::MailCheckClient interface).
*/

#include <glib.h>
#include <orb/orbit.h>
#include "server.h"

void notify_new_mailbox(ClientInfo* client, gchar* mailbox)
{
  CORBA_Object obj;
  obj = CORBA_ORB_string_to_object(CorbaRefs.orb, 
				   client->ior,
				   &(CorbaRefs.ev));
  if(CorbaRefs.ev._major != CORBA_NO_EXCEPTION)
    {
      g_message("Could not notify client %s: received exception %d
converting IOR.\n", client->name, CorbaRefs.ev._major);
    }
  else
    {
      GNOME_MailCheckClient_new_mailbox(obj, mailbox, &(CorbaRefs.ev));
      if(CorbaRefs.ev._major != CORBA_NO_EXCEPTION)
	{
	  g_message("Received exception %d notifying client %s.\n", 
		    CorbaRefs.ev._major, client->name);
	}
    }
  CORBA_free(obj);
}

void notify_delete_mailbox(ClientInfo* client, gchar* mailbox)
{
  CORBA_Object obj;
  obj = CORBA_ORB_string_to_object(CorbaRefs.orb, 
				   client->ior,
				   &(CorbaRefs.ev));
  if(CorbaRefs.ev._major != CORBA_NO_EXCEPTION)
    {
      g_message("Could not notify client %s: received exception %d
converting IOR.\n", client->name, CorbaRefs.ev._major);
    }
  else
    {
      GNOME_MailCheckClient_delete_mailbox(obj, mailbox, &(CorbaRefs.ev));
      if(CorbaRefs.ev._major != CORBA_NO_EXCEPTION)
	{
	  g_message("Received exception %d notifying client %s.\n", 
		    CorbaRefs.ev._major, client->name);
	}
    }
  CORBA_free(obj);
}


/* the dummy parameter is needed to allow call through g_list_foreach() */
  
void notify_server_shutdown(ClientInfo* client, gchar* dummy)
{
  CORBA_Object obj;
  obj = CORBA_ORB_string_to_object(CorbaRefs.orb, 
				   client->ior,
				   &(CorbaRefs.ev));
  if(CorbaRefs.ev._major != CORBA_NO_EXCEPTION)
    {
      g_message("Could not notify client %s: received exception %d
converting IOR.\n", client->name, CorbaRefs.ev._major);
    }
  else
    {
      GNOME_MailCheckClient_server_shutdown(obj, &(CorbaRefs.ev));
      if(CorbaRefs.ev._major != CORBA_NO_EXCEPTION)
	{
	  g_message("Received exception %d notifying client %s.\n", 
		    CorbaRefs.ev._major, client->name);
	}
    }
  CORBA_free(obj);
}



