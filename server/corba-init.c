/* ORBit stuff */
/* based on ORBit/docs/Using-POA.txt in CVS */

#include <libgnorba/gnorba.h>
#include <glib.h>
#include "server.h"

POA_GNOME_MailCheckServer__epv my_MailCheckServer_epv = {
  NULL,
  &do_GNOME_MailCheckServer_list_mailboxes,
  &do_GNOME_MailCheckServer_list_mailbox_types,
  &do_GNOME_MailCheckServer_register_client,
  &do_GNOME_MailCheckServer_unregister_client,
  &do_GNOME_MailCheckServer_set_monitor_status,
  &do_GNOME_MailCheckServer_get_monitor_status,
  &do_GNOME_MailCheckServer_create_mailbox,
  &do_GNOME_MailCheckServer_destroy_mailbox,
  &do_GNOME_MailCheckServer_set_check_interval,
  &do_GNOME_MailCheckServer_get_check_interval,
  &do_GNOME_MailCheckServer_authenticate_mailbox
};

PortableServer_ServantBase__epv base_epv = {
  NULL,				/* ORB-private data */
  NULL,				/* servant finalization func */
  NULL				/* default_POA func */
};

POA_GNOME_MailCheckServer__vepv MailCheckServer_vepv = {
  &base_epv,
  &my_MailCheckServer_epv
};

POA_GNOME_MailCheckServer my_MailCheckServer_servant = {
  NULL,				/* ORB-private data */
  &MailCheckServer_vepv		/* pointer to list of function tables */
};

/* CORBA initialization code */

/* initialize the servant, tell the ORB to send requests to the
   servant, and return an object reference for the servant */

gchar* create_GNOME_MailCheckServer(CORBA_ORB orb,
				   PortableServer_POA poa, 
				   CORBA_Environment *ev)
{
  gchar* retval;
  CORBA_Object obj;
  
  /* Create an ObjectId for our new CORBA object */
  PortableServer_ObjectId objid = {
    0,				/* always zero */
    sizeof("GNOME_MailCheckServer"),
    "GNOME_MailCheckServer"
  };

  /* ask the ORB to fill in servant structure so it can handle requests */
  POA_GNOME_MailCheckServer__init(&my_MailCheckServer_servant, ev);
  
  /* tell the POA to send requests for this objid to our servant */
  PortableServer_POA_activate_object_with_id(poa, &objid, 
					     &my_MailCheckServer_servant, ev);
  
  obj = PortableServer_POA_servant_to_reference(poa, 
						&my_MailCheckServer_servant,
						ev);
  
  retval = CORBA_ORB_object_to_string(orb, obj, ev);
  
  CORBA_Object_release(obj, ev);
  
  /* tell the POA manager to start taking incoming requests for the
     POA that our servant is using */

  PortableServer_POAManager_activate(PortableServer_POA__get_the_POAManager(poa,
									   ev),
				     ev);

  /* tell the world how to access our object */
  
  return retval;
}

void destroy_GNOME_MailCheckServer(CORBA_ORB orb,
				   PortableServer_POA poa,
				   CORBA_Environment* ev)
{
  PortableServer_ObjectId objid = {
    0, 
    sizeof("GNOME_MailCheckServer"), 
    "GNOME_MailCheckServer" 
  };
  
  PortableServer_POA_deactivate_object(poa, &objid, ev);
  
  POA_GNOME_MailCheckServer__fini(&my_MailCheckServer_servant, ev);
}

gchar* init_CORBA(gint* argc, gchar** argv)
{
  CORBA_exception_init(&(CorbaRefs.ev));
  CorbaRefs.orb = gnome_CORBA_init("GNOME_MailCheckServer", NULL, argc, argv, 
				   0, &(CorbaRefs.ev));
  CorbaRefs.poa = CORBA_ORB_resolve_initial_references(CorbaRefs.orb, 
						       "RootPOA", 
						       &(CorbaRefs.ev));
  
  return create_GNOME_MailCheckServer(CorbaRefs.orb, CorbaRefs.poa, 
				      &(CorbaRefs.ev));
}
			 



