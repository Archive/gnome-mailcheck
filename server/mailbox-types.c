/*
Definitions of mailbox types supported by server
Copyright (C) Russell Steinthal 1999
*/

#include <glib.h>
#include "server.h"

#define MAXTYPES 10

int num_types = 0;
MailboxType* mailbox_types[MAXTYPES];

void define_mailbox_type(gchar* name, PollFunc poll, TryAuthFunc auth)
{
  MailboxType* newType;
  if(num_types >= MAXTYPES) 
    g_error("Cannot define mailbox type %s- increase MAXTYPES!\n", name);
  newType = g_new(MailboxType, 1);
  newType->name = g_memdup(name,strlen(name)+1);
  newType->poll_func = poll;
  newType->try_auth_func = auth;
  mailbox_types[num_types++] = newType;
}

gint find_mailbox_type(gchar* name)
{
  int i;
  for(i=0;i<num_types;i++)
    {
      if(!strcmp(mailbox_types[i]->name, name)) return i;
    }
  return -1;
}

void init_mailbox_types()
{
  /* To define a new mailbox type, add a call to define_mailbox_type
     *after* the existing calls listed here */
  
  define_mailbox_type("dummy1", NULL, NULL);
  define_mailbox_type("dummy2", NULL, NULL);
  define_mailbox_type("lastdummy", NULL, NULL);

  /*define_mailbox_type("mbox", poll_mbox);*/
}


  









