/*
libmailcheck.c
Copyright (C) Russell Steinthal (rms39@columbia.edu) 1999
For the GNOME Project
*/

#include <glib.h>
#include <orb/orb.h>
#include "libmailcheck.h"
#include "client-corba.h"

static GnomeMailCheck mc;
gchar* gnome_mailcheck_corba_init(gchar* name, GnomeMailCheck* _mc);

gboolean check_exceptions(gchar* context, CORBA_Environment* ev)
{
  if(ev->_major != CORBA_NO_EXCEPTION)
    {
      g_warning("%s: Received exception %d.\n", context, ev->_major);
      return FALSE;
    }
  return TRUE;
}

GnomeMailCheck* gnome_mailcheck_init(void)
{
  return gnome_mailcheck_init_with_auth_func(NULL);
}

GnomeMailCheck* gnome_mailcheck_init_with_auth_func(GnomeAuthFunc func)
{
  if(!gnome_mailcheck_corba_init("libmailcheck", &mc))
    {
      g_error("Could not initialize client-side CORBA!");
    }
  mc.default_auth_func = func;
  return &mc;
}
  
gboolean gnome_mailcheck_quit(void)
{
  /* Unregister the client */
  CORBA_boolean res;
  res = GNOME_MailCheckServer_unregister_client(mc.corba_server, 
						mc.client_id, &(mc.ev));

  /* FIX ME??? free resources stored in GnomeMailCheck structure */
  return (gboolean) res;
}

GList* gnome_mailcheck_list_mailboxes(void)
{
  GList* ret = corba_seq_to_glist(GNOME_MailCheckServer_list_mailboxes(mc.corba_server, &(mc.ev)));
  return (check_exceptions("gnome_mailcheck_list_mailboxes", &(mc.ev))
	  ? ret : NULL);
	  
}

GList* gnome_mailcheck_list_mailbox_types(void)
{
 GList* ret = corba_seq_to_glist(GNOME_MailCheckServer_list_mailbox_types(mc.corba_server, &(mc.ev)));
  return (check_exceptions("gnome_mailcheck_list_mailbox_types", &(mc.ev))
	  ? ret : NULL);
}

gboolean gnome_mailcheck_define_mailbox(gchar* type, gchar* name)
{
  GNOME_MailCheckServer_Error err;
  err = GNOME_MailCheckServer_create_mailbox(mc.corba_server, type, 
					     name, &(mc.ev));
  return (check_exceptions("gnome_mailcheck_define_mailbox", &(mc.ev)) &&
	  ((err == GNOME_MailCheckServer_OK) ? TRUE : FALSE));
}

gboolean gnome_mailcheck_undefine_mailbox(gchar* name)
{
  CORBA_boolean res =  GNOME_MailCheckServer_destroy_mailbox(mc.corba_server,
							     name, &(mc.ev));
  return check_exceptions("gnome_mailcheck_undefine_mailbox", &(mc.ev)) && res;
}

guint gnome_mailcheck_get_check_interval(gchar* name)
{
  guint ret = GNOME_MailCheckServer_get_check_interval(mc.corba_server,
						       name, &(mc.ev));
  return (check_exceptions("gnome_mailcheck_get_check_interval", &(mc.ev)) 
	  ? ret : 0);
}

gboolean gnome_mailcheck_set_check_interval(gchar* name, guint secs)
{
  gboolean res;
  res = (gboolean) GNOME_MailCheckServer_set_check_interval(mc.corba_server,
							    name, secs,
							    &(mc.ev));
  return (check_exceptions("gnome_mailcheck_set_check_interval", &(mc.ev)) && 
	  res);
}

GnomeAuthFunc gnome_mailcheck_get_default_auth_func(void)
{
  return mc.default_auth_func;
}

GnomeAuthFunc gnome_mailcheck_set_default_auth_func(GnomeAuthFunc func)
{
  GnomeAuthFunc old = mc.default_auth_func;
  mc.default_auth_func = func;
  return old;
}

gboolean gnome_mailcheck_set_monitor_status(gchar* name, gboolean status)
{
  gboolean res;
  res = (gboolean) GNOME_MailCheckServer_set_monitor_status(mc.corba_server,
							    mc.client_id,
							    name, 
							    status,
							    &(mc.ev));
  return (check_exceptions("gnome_mailcheck_set_monitor_status", &(mc.ev)) 
	  && res);
}

gboolean gnome_mailcheck_get_monitor_status(gchar* name)
{
  gboolean res;
  res = (gboolean) GNOME_MailCheckServer_get_monitor_status(mc.corba_server,
							    mc.client_id,
							    name, 
							    &(mc.ev));
  return (check_exceptions("gnome_mailcheck_get_monitor_status", &(mc.ev)) && 
	  res);
}








  
  
  






