#include <gtk/gtk.h>
#include <libgnorba/gnorba.h>
#include <orb/orbit.h>
#include "client-corba.h"
#include "mailcheck.h"
#include "GnomeMailCheck.h"
#include "GnomeMailbox.h"

static CORBA_ORB orb = NULL;
static GnomeMailCheck* mc;

static POA_GNOME_MailCheckClient__epv client_epv = {
  NULL,
  &do_GNOME_MailCheckClient_notify,
  &do_GNOME_MailCheckClient_new_mailbox,
  &do_GNOME_MailCheckClient_delete_mailbox,
  &do_GNOME_MailCheckClient_server_shutdown,
  &do_GNOME_MailCheckClient_authenticate_mailbox
};

static PortableServer_ServantBase__epv base_epv = {
  NULL,				/* ORB-private data */
  NULL,				/* servant finalization func */
  NULL				/* default_POA func */
};

POA_GNOME_MailCheckClient__vepv client_vepv = {
  &base_epv,	
  &client_epv
};

POA_GNOME_MailCheckClient client_servant = {
  NULL,				/* ORB-private data */
  &client_vepv
};

/* 
Connect with the CORBA server, register the client, 
and return an IOR to the server 

Lots of this code is cribbed from capplet_widget_corba_init() from the
control-center tree.
*/

gchar* gnome_mailcheck_corba_init(gchar* name, GnomeMailCheck* _mc)
{
  PortableServer_POA poa;
  GNOME_MailCheckServer server;
  gchar* client_ior = NULL;
  
  int dummy_argc = 0;
  char* dummy_argv[] = { "libmailcheck" };

  CORBA_exception_init(&(_mc->ev));
  
  mc = _mc;

  /* Note: this is a potential bug if gnome_CORBA_init is not
     idempotent, since the client might have already called gnome_init
     before we get here */

  /* argc / argv are dummied up - could pass real values into this
     function if desired */
  
  if(!orb)
    {
      mc->orb = orb = gnome_CORBA_init(name, VERSION, 
				       &dummy_argc, dummy_argv, 0, NULL);
    }

  if(!orb)
    {
      g_error("Could not connect to CORBA server (could not obtain ORB reference).\n");
    }
  
  mc->poa = poa = CORBA_ORB_resolve_initial_references(orb, "RootPOA", &(mc->ev));
  
  client_ior = create_GNOME_MailCheckClient(orb, poa, &(mc->ev));
  
  /* Now that we have our IOR determined, we need to find the MailCheckServer */
  
  server = goad_server_activate_with_repo_id(NULL, "IDL:GNOME/MailCheckServer:1.0",
					     0, NULL);
  if(!server)
    {
      g_error("Could not connect to MailCheckServer.\n");
    }
  
  g_message("Registering with server...\n");
  mc->client_id = GNOME_MailCheckServer_register_client(server, name, 
							client_ior, &(mc->ev));
  if(mc->ev._major != CORBA_NO_EXCEPTION)
    {
      g_error("We receieved an exception %d from register_client()!\n", mc->ev._major);
    }
  
  /* 
     mc->corba_server = CORBA_ORB_object_to_string(orb, server, &(mc->ev));
     CORBA_Object_release(server, &(mc->ev)); 
     return mc->corba_server; 
  */
  
  mc->corba_server = server;
  return CORBA_ORB_object_to_string(orb, server, &(mc->ev));
}
  
gchar* create_GNOME_MailCheckClient(CORBA_ORB orb, PortableServer_POA poa, 
				    CORBA_Environment* ev)
{
  gchar* retval;
  CORBA_Object obj;
  
  /* Create an ObjectId for our new CORBA object */
  PortableServer_ObjectId objid = {
    0,				/* always zero */
    sizeof("GNOME_MailCheckClient"),
    "GNOME_MailCheckClient"
  };

  /* ask the ORB to fill in servant structure so it can handle requests */
  POA_GNOME_MailCheckClient__init(&client_servant, ev);
  
  /* tell the POA to send requests for this objid to our servant */
  PortableServer_POA_activate_object_with_id(poa, &objid, &client_servant, ev);
  
  obj = PortableServer_POA_servant_to_reference(poa, &client_servant, ev);
  
  retval = CORBA_ORB_object_to_string(orb, obj, ev);
  
  CORBA_Object_release(obj, ev);
  
  /* tell the POA manager to start taking incoming requests for the
     POA that our servant is using */

  PortableServer_POAManager_activate(PortableServer_POA__get_the_POAManager(poa,
									   ev),
				     ev);

  /* tell the world how to access our object */
  
  return retval;
}

void destroy_GNOME_MailCheckClient(PortableServer_POA poa, 
				   CORBA_Environment* ev)
{
  PortableServer_ObjectId objid = {
    0, 
    sizeof("GNOME_MailCheckClient"), 
    "GNOME_MailCheckClient" 
  };
  
  PortableServer_POA_deactivate_object(poa, &objid, ev);
  
  POA_GNOME_MailCheckClient__fini(&client_servant, ev);
}

void do_GNOME_MailCheckClient_notify(PortableServer_Servant servant,
				     const CORBA_char* mailbox, 
				     const CORBA_short numNew,
				     const CORBA_short numTotal, 
				     CORBA_Environment* ev)
{
  GnomeMailbox* box = get_mailbox_by_name((gchar*)mailbox);
  gtk_signal_emit_by_name(GTK_OBJECT(box), "new_mail", numNew, numTotal);
}

void do_GNOME_MailCheckClient_new_mailbox(PortableServer_Servant servant,
					  const CORBA_char* mailbox,
					  CORBA_Environment* ev)
{
  if(mc) gtk_signal_emit_by_name(GTK_OBJECT(mc), "new_mailbox", mailbox);
}

void do_GNOME_MailCheckClient_delete_mailbox(PortableServer_Servant servant,
					     const CORBA_char* mailbox, 
					     CORBA_Environment* ev)
{
  GnomeMailbox* box = get_mailbox_by_name((gchar*)mailbox);
  if(mc) gtk_signal_emit_by_name(GTK_OBJECT(mc), "delete_mailbox", mailbox);
  gtk_signal_emit_by_name(GTK_OBJECT(box), "mailbox_undefined");
}

void do_GNOME_MailCheckClient_server_shutdown(PortableServer_Servant servant,
					      CORBA_Environment* ev)
{
  if(mc) gtk_signal_emit_by_name(GTK_OBJECT(mc), "server_shutdown");
}

void do_GNOME_MailCheckClient_authenticate_mailbox(PortableServer_Servant serv,
						   const CORBA_char* mailbox,
						   CORBA_Environment* ev)
{
  GnomeMailbox* box = get_mailbox_by_name((gchar*)mailbox);
  gtk_signal_emit_by_name(GTK_OBJECT(box), "authenticate");
}

    







