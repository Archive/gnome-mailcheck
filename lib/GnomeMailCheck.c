/*
Implementation of GnomeMailCheck object
Copyright (C) Russell Steinthal (rms39@columbia.edu) 1999
*/

#include "GnomeMailCheck.h"

static void gnome_mailcheck_class_init(GnomeMailCheckClass* klass);
static void gnome_mailcheck_init(GnomeMailCheck* mc);

enum { 
  NEWMAILBOX_SIGNAL, 
  DELETEMAILBOX_SIGNAL, 
  SHUTDOWN_SIGNAL, 
  LAST_SIGNAL 
};

/* should this be 4 zeroes (as in control-center) or 1 (as in tutorial)? */
static gint mailchecker_signals[] = { 0, 0, 0, 0 };

guint gnome_mailcheck_get_type(void)
{
  static guint mailchecker_type = 0;
  if(!mailchecker_type)
    {
      GtkTypeInfo mailchecker_info =
      {
	"GnomeMailCheck",
	sizeof(GnomeMailCheck),
	sizeof(GnomeMailCheckClass),
	(GtkClassInitFunc) gnome_mailcheck_class_init,
	(GtkObjectInitFunc) gnome_mailcheck_init,
	(GtkArgSetFunc) NULL,	/* FIX ME??? */
	(GtkArgGetFunc) NULL    /* FIX ME??? */
      };
      mailchecker_type = gtk_type_unique(gtk_object_get_type(), &mailchecker_info);
    }

  return mailchecker_type;
}

static void gnome_mailcheck_class_init(GnomeMailCheckClass* klass)
{
  GtkObjectClass* object_class;
  object_class = (GtkObjectClass*) klass;
  mailchecker_signals[NEWMAILBOX_SIGNAL] = 
    gtk_signal_new("new_mailbox", GTK_RUN_LAST, object_class->type,
		   GTK_SIGNAL_OFFSET(GnomeMailCheckClass, new_mailbox),
		   gtk_marshal_NONE__STRING,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
  mailchecker_signals[DELETEMAILBOX_SIGNAL] = 
    gtk_signal_new("delete_mailbox", GTK_RUN_LAST, object_class->type,
		   GTK_SIGNAL_OFFSET(GnomeMailCheckClass, delete_mailbox),
		   gtk_marshal_NONE__STRING,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
  mailchecker_signals[SHUTDOWN_SIGNAL] = 
    gtk_signal_new("shutdown", GTK_RUN_LAST, object_class->type,
		   GTK_SIGNAL_OFFSET(GnomeMailCheckClass, shutdown),
		   gtk_signal_default_marshaller, /* ??? */
		   GTK_TYPE_NONE, 0);
  
  gtk_object_class_add_signals(object_class,mailchecker_signals,LAST_SIGNAL);
  
  klass->new_mailbox = NULL;
  klass->delete_mailbox = NULL;
  klass->shutdown = NULL;
}

static void gnome_mailcheck_init(GnomeMailCheck* mc)
{
  /* Nothing to do! */
}


  

			     

		   
							  


