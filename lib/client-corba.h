#ifndef CLIENT_CORBA_H
#define CLIENT_CORBA_H

#include "mailcheck.h"

#define VERSION "0.1.0"

typedef gchar* (*ExtractFunc)(void* obj);

gchar* create_GNOME_MailCheckClient(CORBA_ORB orb, PortableServer_POA poa, 
				    CORBA_Environment* ev);

void destroy_GNOME_MailCheckServer(PortableServer_POA poa,  CORBA_Environment* ev);

void do_GNOME_MailCheckClient_notify(PortableServer_Servant servant,
				     const CORBA_char* mailbox, 
				     const CORBA_short numNew,
				     const CORBA_short numTotal, 
				     CORBA_Environment* ev);

void do_GNOME_MailCheckClient_new_mailbox(PortableServer_Servant servant,
					  const CORBA_char* mailbox,
					  CORBA_Environment* ev);

void do_GNOME_MailCheckClient_delete_mailbox(PortableServer_Servant servant,
					     const CORBA_char* mailbox, 
					     CORBA_Environment* ev);

void do_GNOME_MailCheckClient_server_shutdown(PortableServer_Servant servant,
					      CORBA_Environment* ev);

void do_GNOME_MailCheckClient_server_shutdown(PortableServer_Servant servant,
					      CORBA_Environment* ev);

void do_GNOME_MailCheckClient_authenticate_mailbox(PortableServer_Servant serv,
						   const CORBA_char* mailbox,
						   CORBA_Environment* ev);
/* sequence utility routines */

GNOME_MailCheckServer_stringList* 
array_to_corba_seq(void* array[], int num, ExtractFunc func);
GNOME_MailCheckServer_stringList* 
glist_to_corba_seq(GList* list, ExtractFunc func);
GList* corba_seq_to_glist(GNOME_MailCheckServer_stringList* seq);

#endif




