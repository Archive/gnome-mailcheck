/*
  Routines for handling CORBA sequences
  Copyright (C) Russell Steinthal (rms39@columbia.edu) 1999
  Licensed under the GPL; see COPYING for details

  Adapted from code provided by Michael Lausch (mla@gams.co.at) on
  orbit-list and by private mail
*/

#include <glib.h>
#include "mailcheck.h"

typedef gchar* (*ExtractFunc)(void* obj);

gpointer identity_func(gpointer p)
{
  return p;
}

GNOME_MailCheckServer_stringList* 
array_to_corba_seq(void** array, int num, ExtractFunc func)
{
  int i; 
  GNOME_MailCheckServer_stringList* retval; 
  CORBA_char** buf; 
  if(!func) func = identity_func;
  buf = CORBA_sequence_CORBA_string_allocbuf(num); 
  for(i=0;i<num;i++) 
    { 
      buf[i] = CORBA_string_alloc(strlen(func(array[i])));
      strcpy(buf[i], func(array[i]));
    }
  
  retval = GNOME_MailCheckServer_stringList__alloc();
  retval->_length = num;
  retval->_buffer = buf;
  return retval;
}
  
GNOME_MailCheckServer_stringList* 
glist_to_corba_seq(GList* list, ExtractFunc func)
{
  int i=0, length;
  GNOME_MailCheckServer_stringList* retval = NULL;
  CORBA_char** buf;
  if(!func) func = identity_func;
  length = g_list_length(list);
  buf = CORBA_sequence_CORBA_string_allocbuf(length);
  for(i=0;list;i++, list=g_list_next(list))
    {
      buf[i] = CORBA_string_alloc(strlen(func(list->data)));
      strcpy(buf[i], func(list->data));
    }

  retval = GNOME_MailCheckServer_stringList__alloc();
  retval->_length = length;
  retval->_buffer = buf;
  return retval;
}

GList* corba_seq_to_glist(GNOME_MailCheckServer_stringList* seq)
{
  GList* list = NULL;
  int i;
  for(i=0;i<seq->_length;i++)
    {
      list = g_list_append(list, seq->_buffer[i]);
    }
  return list;
}









