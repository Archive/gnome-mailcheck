#ifndef __GNOMEMAILCHECK_H__
#define __GNOMEMAILCHECK_H__

#include <gtk/gtk.h>
#include <orb/orbit.h>
#include "mailcheck.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GNOMEMAILCHECK(obj) GTK_CHECK_CAST(obj,gnome_mailcheck_get_type(), GnomeMailCheck)
#define GNOMEMAILCHECK_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gnome_mailcheck_get_type(), GnomeMailCheckClass)
#define IS_GNOMEMAILCHECK(obj) GTK_CHECK_TYPE(obj, gnome_mailcheck_get_type())

typedef struct _GnomeMailCheck GnomeMailCheck;
typedef struct _GnomeMailCheckClass GnomeMailCheckClass;

typedef gchar* (*GnomeAuthFunc)(gpointer data);

struct _GnomeMailCheck
{
  GtkObject gtk_object;
  GnomeAuthFunc default_auth_func;
  CORBA_ORB orb;
  PortableServer_POA poa;
  CORBA_Environment ev;
  /*gchar* corba_server;*/
  GNOME_MailCheckServer corba_server;
  GNOME_MailCheckServer_ClientID client_id;
};

struct _GnomeMailCheckClass
{
  GtkObjectClass parent_class;
  void (*new_mailbox)(GnomeMailCheck* checker, gchar* mailbox);
  void (*delete_mailbox)(GnomeMailCheck* checker, gchar* mailbox);
  void (*shutdown)(GnomeMailCheck* checker);
};

guint      gnome_mailcheck_get_type(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GNOMEMAILCHECK_H__ */


