#ifndef __GNOMEMAILBOX_H__
#define __GNOMEMAILBOX_H__

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GNOMEMAILBOX(obj) GTK_CHECK_CAST(obj, gnome_mailbox_get_type(), GnomeMailbox)
#define GNOMEMAILBOX_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gnome_mailbox_get_type(), GnomeMailboxClass)
#define IS_GNOMEMAILBOX(obj) GTK_CHECK_TYPE(obj, gnome_mailbox_get_type())

typedef struct _GnomeMailbox GnomeMailbox;
typedef struct _GnomeMailboxClass GnomeMailboxClass;

typedef void     (*GnomePollFunc)      (guint* numNew, guint* total);

struct _GnomeMailbox
{
  GtkObject gtk_object;		  /* base class object */
  gchar* name;			  /* the mailbox name */
  /* any additional Mailbox/CORBA stuff? */
};

struct _GnomeMailboxClass
{
  GtkObjectClass parent_class;
  void (*new_mail)(GnomeMailbox* box, gint new, gint total);
  void (*mailbox_undefined)(GnomeMailbox* box);
  void (*authenticate)(GnomeMailbox* box);
};

guint gnome_mailbox_get_type(void);
GtkObject* gnome_mailbox_new(gchar* name);
GnomeMailbox* get_mailbox_by_name(gchar* name);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GNOMEMAILBOX_H__ */







