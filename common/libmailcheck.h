#ifndef LIBMAILCHECK_H
#define LIBMAILCHECK_H

#include "GnomeMailbox.h"
#include "GnomeMailCheck.h"

/* Prototypes */

struct _GnomeMailCheck* gnome_mailcheck_init(void);
struct _GnomeMailCheck* gnome_mailcheck_init_with_auth_func(GnomeAuthFunc func);

gboolean        gnome_mailcheck_quit(void);

GList*          gnome_mailcheck_list_mailboxes(void);
GList*          gnome_mailcheck_list_mailbox_types(void);

gboolean        gnome_mailcheck_define_mailbox(gchar* type, gchar* name);
gboolean        gnome_mailcheck_undefine_mailbox(gchar* name);

guint           gnome_mailcheck_get_check_interval(gchar* name);
gboolean        gnome_mailcheck_set_check_interval(gchar* name, guint secs);

GnomeAuthFunc   gnome_mailcheck_get_default_auth_func(void);
GnomeAuthFunc   gnome_mailcheck_set_default_auth_func(GnomeAuthFunc func);

gboolean        gnome_mailcheck_set_monitor_status(gchar* name, 
						   gboolean status);

gboolean        gnome_mailcheck_get_monitor_status(gchar* name);

void            gnome_mailcheck_default_auth_handler(GnomeMailbox* box);

#endif



					       
