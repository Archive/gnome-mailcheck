/*
GNOMEMailbox widget - derived from GtkObject
Copyright (C) Russell Steinthal (rms39@columbia.edu) 1999
*/

#include <gtk/gtk.h>
#include "GnomeMailbox.h"
#include "libmailcheck.h"

static void gnome_mailbox_class_init(GnomeMailboxClass* klass);
static void gnome_mailbox_init(GnomeMailbox* box);
GtkObject* gnome_mailbox_new(gchar* box_name);
gint mailbox_name_comp(gconstpointer a, gconstpointer b);
void mailbox_close_callback(void);

static GList* mailbox_list = NULL;

enum {
  NEWMAIL_SIGNAL,
  DELETE_MAILBOX_SIGNAL,
  AUTHENTICATE_SIGNAL,
  LAST_SIGNAL
};

static int mailbox_signals[] = { 0, 0, 0, 0 };

guint gnome_mailbox_get_type(void)
{
  static guint mailbox_type = 0;
  if(!mailbox_type)
    {
      GtkTypeInfo mailbox_info =
      {
	"GnomeMailbox",
	sizeof(GnomeMailbox),
	sizeof(GnomeMailboxClass),
	(GtkClassInitFunc) gnome_mailbox_class_init,
	(GtkObjectInitFunc) gnome_mailbox_init,
	(GtkArgSetFunc) NULL,	/* FIX ME??? */
	(GtkArgGetFunc) NULL    /* FIX ME??? */
      };

      mailbox_type = gtk_type_unique(gtk_object_get_type(), &mailbox_info);
    }

  return mailbox_type;
}

static void gnome_mailbox_class_init(GnomeMailboxClass* klass)
{
  GtkObjectClass* object_class;
  object_class = (GtkObjectClass*) klass;
  mailbox_signals[NEWMAIL_SIGNAL] =
    gtk_signal_new("new_mail", GTK_RUN_LAST, object_class->type,
		   GTK_SIGNAL_OFFSET(GnomeMailboxClass, new_mail),
		   gtk_marshal_NONE__INT_INT,
		   GTK_TYPE_NONE, 2, GTK_TYPE_INT, GTK_TYPE_INT);
  mailbox_signals[DELETE_MAILBOX_SIGNAL] =
    gtk_signal_new("mailbox_undefined", GTK_RUN_LAST, object_class->type,
		   GTK_SIGNAL_OFFSET(GnomeMailboxClass, mailbox_undefined),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  mailbox_signals[AUTHENTICATE_SIGNAL] =
    gtk_signal_new("authenticate", GTK_RUN_LAST, object_class->type,
		   GTK_SIGNAL_OFFSET(GnomeMailboxClass, authenticate),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals(object_class, mailbox_signals, LAST_SIGNAL);

  klass->new_mail = NULL;
  klass->mailbox_undefined = NULL;
  klass->authenticate = gnome_mailcheck_default_auth_handler;
}

static void gnome_mailbox_init(GnomeMailbox* box)
{
  mailbox_list = g_list_prepend(mailbox_list, box);
  gtk_signal_connect(GTK_OBJECT(box), "destroy",
		     (GtkSignalFunc) mailbox_close_callback, NULL);
}

GtkObject* gnome_mailbox_new(gchar* box_name)
{
  GnomeMailbox* retval;
  retval = GNOMEMAILBOX(gtk_type_new(gnome_mailbox_get_type()));
  retval->name = g_strdup(box_name);
  /* tell CORBA server we're monitoring mailbox */
  gnome_mailcheck_set_monitor_status(box_name, TRUE);
  return GTK_OBJECT(retval);
}

int mailbox_compare(GnomeMailbox* box, gchar* name)
{
  return strcmp(box->name, name);
}

GnomeMailbox* get_mailbox_by_name(gchar* name)
{
  return (g_list_find_custom(mailbox_list, name, mailbox_name_comp))->data;
}

void mailbox_close_callback(void)
{
  /* callback for "destroy" signal" */
}


  
  

  

